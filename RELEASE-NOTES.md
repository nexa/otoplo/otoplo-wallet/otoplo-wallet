# Release notes

## 3.1.0 (03 Mar 2025)

- Added NRC-1 tokens support.
- Added Nebula / NRC-3 NFTs support.
- Handle IPFS links for token description and icons.
- Performance optimization and bug fixes.

## 3.0.0 (02 Dec 2024)

### 🚀 **Major Release**
This release marks a significant milestone with a wide range of improvements, new features, and changes. It includes updates across both mobile and desktop platforms, enhancing performance, adding new functionality, and fixing known issues.

---

- Added support for Audio and Video NFTs.
- The price ticker now includes percentage changes to provide a quick visual reference for performance.
- Fixed a rare issue with wallet discovery index.

### **Mobile-Specific Changes**
- Added an option to use Biometric Auth (Face ID / Fingerprint) as an alternative to your password.
- Choose whether to require authentication (password or biometrics) for sensitive actions.
- You can now export and share NFTs and transaction history.
- Improved performance on low-end devices.
- Fixed a small issue in the QR Scanner layout.

### **Desktop-Specific Changes**
- Fixed an issue with cache size limit.

## 2.2.0 (03 Nov 2024)

* Add fiat currency options for balance value display
* Desktop: Make updater more strict when identify new version
* chore: Add AppImage support for Linux

## 2.1.0 (28 Dec 2023)

* Desktop: Add option to export transactions to CSV file

## 2.0.1 (26 Dec 2023)

* Mobile: Fix crashing issue on old devices

## 2.0.0 (17 Nov 2023)

* Add Tokens and NFTs support
* Add transaction details page
* Add toast notifications
* Add option to select custom Rostrum instance
* Add option to rescan transactions and erase wallet data
* Refresh UI layout with better UX
* Improve blockchain scan performance
* Optimize UTXO selection operation
* Fix rare case of incorrect balance after login
* Fix 'tx-conflict' issue when sending multiple transactions

## 1.4.0 (17 May 2023)

* Add HODL Vault
* Add UTXO consolidation
* Add sidebar menu and make UI more simple
* Support BIP21 on QR scan
* Increase address scanning bulk to 20
* Update application icons
* Update ticker to NEXA
* Performance optimization and bug fixes

## 1.3.2 (06 Mar 2023)

* Change UI to Otoplo color scheme

## 1.3.1 (21 Feb 2023)

* Remove loading screen
* Show first address in case of offline mode

## 1.3.0 (21 Feb 2023)

* Rebrand to Otoplo

## 1.2.1 (08 Feb 2023)

* Add progress circle for transaction confiramtion

## 1.2.0 (07 Feb 2023)

* Add NEXA/USDT price value and also show the Available amount in USDT($)

## 1.1.0 (06 Feb 2023)

* Add receiving address as QR code
* Add the option to scan QR for sending Nexa