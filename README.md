# Otoplo Wallet

Otoplo Wallet is a free, open-source modern Nexa non-custodial wallet application for Windows, Linux, Mac and Mobile (iOS and Android). Release binaries are available directly from [Otoplo Website](https://otoplo.com/download).

## Building for Desktop

Clone the repo and open the directory:

```sh
git clone https://gitlab.com/nexa/otoplo/otoplo-wallet/otoplo-wallet.git
cd otoplo-wallet
```

Install dependencies and compile:

```sh
npm install
npm run make:desktop
```

The binaries will be created in `out` directory.
