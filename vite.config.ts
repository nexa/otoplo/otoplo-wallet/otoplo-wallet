import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';
import svgr from 'vite-plugin-svgr';
import electron from 'vite-plugin-electron';
import { nodePolyfills } from 'vite-plugin-node-polyfills';

const shouldRunElectron = process.env.VITE_IS_DESKTOP == 'true' || process.env.NODE_ENV == 'development';

export default defineConfig({
  base: './',
  plugins: [
    react(),
    svgr(),
    nodePolyfills({
      protocolImports: true,
      globals: {
        global: true,
        process: true,
        Buffer: true
      }
    }),
    shouldRunElectron && electron([
      {
        vite: {
          build: {
            outDir: 'dist',
            lib: {
              entry: "electron/main.ts",
              formats: ['es'],
              fileName: () => '[name].js'
            }
          }
        },
      },
      {
        vite: {
          build: {
            outDir: 'dist',
            lib: {
              entry: "electron/preload.ts",
              formats: ['es'],
              fileName: () => '[name].mjs'
            }
          }
        },
      }
    ])
  ],
  server: { // dev server
    port: 3000,
    proxy: {
      "/_public": {
        target: 'https://niftyart.cash',
        changeOrigin: true,
      }
    },
    open: !shouldRunElectron
  }
})