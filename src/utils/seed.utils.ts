import * as Bip39 from 'bip39';
import StorageProvider from '../providers/storage.provider';
import crypto from "crypto";

export function validatePassword(pw: string, pwConfirm: string): string {
    if (pw !== pwConfirm) {
        return "Passwords are not equal."
    }

    if (pw.length < 8) {
        return "Must contain at least 8 or more characters."
    }

    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;

    if (!pw.match(lowerCaseLetters)) {
        return "Must contain a lower case letter."
    }

    if (!pw.match(upperCaseLetters)) {
        return "Must contain an upper case letter."
    }

    if (!pw.match(numbers)) {
        return "Must contain a number."
    }

    return "";
}

export function generateWords() {
    return Bip39.generateMnemonic(128, undefined, Bip39.wordlists.english);
}

export function isMnemonicValid(mnemonic: string) {
    return Bip39.validateMnemonic(mnemonic,  Bip39.wordlists.english);
}

async function derivePBKDF2KeyAndIv(password: string, salt: Buffer): Promise<[Buffer, Buffer]> {
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(password, salt, 100000, 32, 'sha512', (err, key) => {
            if (err) {
                reject(err);
            } else {
                let iv = crypto.createHash('sha256').update(salt).digest().subarray(0, 16);
                resolve([key, iv]);
            }
        });
    });
}

// will be removed in the future
function isHex(str: string) {
    return /^[0-9a-fA-F]+$/.test(str) && str.length % 2 === 0;
}

// will be removed in the future
function decryptOld(encryptedText: string, secret: string) {
    // From https://gist.github.com/schakko/2628689?permalink_comment_id=3321113#gistcomment-3321113
    // From https://gist.github.com/chengen/450129cb95c7159cb05001cc6bdbf6a1
    const cypher = Buffer.from(encryptedText, 'base64');
    const salt = cypher.subarray(8, 16);
    const password = Buffer.concat([Buffer.from(secret), salt]);
    const md5Hashes = [];
    let digest = password;
    for (let i = 0; i < 3; i++) {
      md5Hashes[i] = crypto.createHash('md5').update(digest).digest();
      digest = Buffer.concat([md5Hashes[i], password]);
    }
    const key = Buffer.concat([md5Hashes[0], md5Hashes[1]]);
    const iv = md5Hashes[2];
    const contents = cypher.subarray(16);
    const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
  
    return Buffer.concat([
      decipher.update(contents),
      decipher.final()
    ]).toString('utf8');
  }

export async function decryptMnemonic(encSeed: string, password: string) {
    if (isHex(encSeed)) { // will be removed in the future
        let decMn = decryptOld(Buffer.from(encSeed, 'hex').toString(), crypto.createHash('sha512').update(password, 'utf8').digest('hex'));
        if (decMn && isMnemonicValid(decMn)) {
            await encryptAndStoreMnemonic(decMn, password);
            return decMn;
        }
    } else {
        let encBuf = Buffer.from(encSeed, 'base64');
        let salt = encBuf.subarray(0, 16);
        let contents = encBuf.subarray(16);

        let [key, iv] = await derivePBKDF2KeyAndIv(password, salt);
        let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);

        return Buffer.concat([
            decipher.update(contents),
            decipher.final()
        ]).toString('utf8');
    }
}

export async function encryptAndStoreMnemonic(phrase: string, password: string) {
    let salt = crypto.randomBytes(16);
    let [key, iv] = await derivePBKDF2KeyAndIv(password, salt);
    let cipher = crypto.createCipheriv('aes-256-cbc', key, iv);

    let encBuf = Buffer.concat([salt, cipher.update(phrase), cipher.final()]);
    await StorageProvider.saveEncryptedSeed(encBuf.toString('base64'));
}


export async function isPasswordValid(password: string) {
    try {
        let encSeed = await StorageProvider.getEncryptedSeed();
        if (encSeed) {
            let decMn = await decryptMnemonic(encSeed, password);
            if (decMn && isMnemonicValid(decMn)) {
                return decMn;
            }
        }
        return false;
    } catch {
        return false;
    }
}