import { isValidNexaAddress } from "./wallet.utils";
import { rostrumProvider } from '../providers/rostrum.provider';
import { WalletKeys } from '../models/wallet.entities';
import { MAX_INT64, isNullOrEmpty } from './common.utils';
import { tokenIdToHex } from './token.utils';
import { Address, AddressType, PrivateKey, PublicKey, Script, Transaction, TransactionBuilder, UnitUtils, UTXO } from "libnexa-ts";

const MAX_INPUTS_OUTPUTS = 250;

export interface TxTemplateData {
    templateScript: Script;
    constraintScript: Script;
    visibleArgs: any[];
    publicKey: PublicKey;
}

interface TxOptions {
    isConsolidate?: boolean;
    toChange?: string;
    templateData?: TxTemplateData;
    feeFromAmount?: boolean;
}

export async function buildAndSignTransferTransaction(keys: WalletKeys, toAddr: string, amount: string, feeFromAmount?: boolean, token?: string) {
    let txOptions: TxOptions = {
        feeFromAmount: feeFromAmount
    }

    let txBuilder = prepareTransaction(toAddr, amount, token);
    let tokenPrivKeys: PrivateKey[] = [];
    if (token) {
        tokenPrivKeys = await populateTokenInputsAndChange(txBuilder, keys, token, BigInt(amount));
    }
    let privKeys = await populateNexaInputsAndChange(txBuilder, keys, txOptions);
    return await finalizeTransaciton(txBuilder, privKeys.concat(tokenPrivKeys));
}

export async function buildAndSignConsolidateTransaction(keys: WalletKeys, toChange?: string, templateData?: TxTemplateData) {
    let txOptions: TxOptions = {
        isConsolidate: true,
        toChange: toChange,
        templateData: templateData
    }

    let txBuilder = new TransactionBuilder();
    let privKeys = await populateNexaInputsAndChange(txBuilder, keys, txOptions);
    return await finalizeTransaciton(txBuilder, privKeys);
}

async function populateNexaInputsAndChange(txBuilder: TransactionBuilder, keys: WalletKeys, options: TxOptions) {
    let rKeys = keys.receiveKeys.filter(k => BigInt(k.balance) > 0n);
    let cKeys = keys.changeKeys.filter(k => BigInt(k.balance) > 0n);
    let allKeys = rKeys.concat(cKeys);
    if (isNullOrEmpty(allKeys)) {
        throw new Error("Not enough Nexa balance.");
    }

    let usedKeys = new Map<string, PrivateKey>();
    let origAmount = options.isConsolidate ? 0 : Number(txBuilder.transaction.outputs[0].value);

    for (let key of allKeys) {
        let utxos = await rostrumProvider.getNexaUtxos(key.address);
        for (let utxo of utxos) {
            let input: UTXO = {
                outpoint: utxo.outpoint_hash,
                address: key.address,
                satoshis: utxo.value,
                templateData: options.templateData
            }
            txBuilder.from(input);

            if (!usedKeys.has(key.address)) {
                usedKeys.set(key.address, key.key.privateKey);
            }
            
            if (options.isConsolidate) {
                txBuilder.change(options.toChange ?? keys.receiveKeys[keys.receiveKeys.length - 1].address);
                if (txBuilder.transaction.inputs.length > MAX_INPUTS_OUTPUTS) {
                    return Array.from(usedKeys.values());
                }
            } else {
                let tx = txBuilder.transaction;
                if (tx.inputs.length > MAX_INPUTS_OUTPUTS) {
                    throw new Error("Too many inputs. Consider consolidate transactions or reduce the send amount.");
                }

                let unspent = tx.getUnspentValue();
                if (unspent < 0n) {
                    continue;
                }

                if (unspent == 0n && options.feeFromAmount) {
                    let txFee = tx.estimateRequiredFee();
                    tx.updateOutputAmount(0, origAmount - txFee);
                    return Array.from(usedKeys.values());                  
                }

                txBuilder.change(options.toChange ?? keys.changeKeys[keys.changeKeys.length - 1].address);
                if (options.feeFromAmount) {
                    let hasChange = tx.getChangeOutput();
                    let txFee = tx.estimateRequiredFee();
                    tx.updateOutputAmount(0, origAmount - txFee);

                    // edge case where change added after update
                    if (!hasChange && tx.getChangeOutput()) {
                        txFee = tx.estimateRequiredFee();
                        tx.updateOutputAmount(0, origAmount - txFee);
                    }
                }

                // check again after change output manipulation
                if (tx.getUnspentValue() < tx.estimateRequiredFee()) {
                    // try to add more utxos to satisfy the minimum fee
                    continue;
                }

                return Array.from(usedKeys.values());
            }
        }
    }

    if (options.isConsolidate) {
        if (usedKeys.size > 0) {
            return Array.from(usedKeys.values());
        }
        throw new Error("Not enough Nexa balance.");
    }

    let err = {
        errorMsg: "Not enough Nexa balance.",
        amount: UnitUtils.formatNEXA(txBuilder.transaction.outputs[0].value),
        fee: UnitUtils.formatNEXA(txBuilder.transaction.estimateRequiredFee())
    }

    throw new Error(JSON.stringify(err));
}

function prepareTransaction(toAddr: string, amount: string, token?: string) {
    if (!isValidNexaAddress(toAddr) && !isValidNexaAddress(toAddr, AddressType.PayToPublicKeyHash)) {
        throw new Error('Invalid Address.');
    }
    if ((token && BigInt(amount) < 1n) || (!token && parseInt(amount) < Transaction.DUST_AMOUNT)) {
        throw new Error("The amount is too low.");
    }
    if ((token && BigInt(amount) > MAX_INT64) || (!token && parseInt(amount) > Transaction.MAX_MONEY)) {
        throw new Error("The amount is too high.");
    }

    let builder = new TransactionBuilder();
    if (token) {
        if (!isValidNexaAddress(token, AddressType.GroupIdAddress)) {
            throw new Error('Invalid Token ID');
        }
        if (Address.getOutputType(toAddr) === 0) {
            throw new Error('Token must be sent to script template address');
        }
        builder.to(toAddr, Transaction.DUST_AMOUNT, token, BigInt(amount))
    } else {
        builder.to(toAddr, amount);
    }

    return builder;
}

async function populateTokenInputsAndChange(txBuilder: TransactionBuilder, keys: WalletKeys, token: string, outTokenAmount: bigint) {
    let tokenHex = tokenIdToHex(token);
    let rKeys = keys.receiveKeys.filter(k => Object.keys(k.tokensBalance).includes(tokenHex));
    let cKeys = keys.changeKeys.filter(k => Object.keys(k.tokensBalance).includes(tokenHex));
    let allKeys = rKeys.concat(cKeys);

    if (isNullOrEmpty(allKeys)) {
        throw new Error("Not enough token balance.");
    }

    let usedKeys = new Map<string, PrivateKey>();
    let inTokenAmount = 0n;

    for (let key of allKeys) {
        let utxos = await rostrumProvider.getTokenUtxos(key.address, token);
        for (let utxo of utxos) {
            if (utxo.token_amount < 0) {
                continue;
            }
            txBuilder.from({
                outpoint: utxo.outpoint_hash,
                address: key.address,
                satoshis: utxo.value,
                groupId: utxo.group,
                groupAmount: BigInt(utxo.token_amount),
            });

            inTokenAmount = inTokenAmount + BigInt(utxo.token_amount);
            if (!usedKeys.has(key.address)) {
                usedKeys.set(key.address, key.key.privateKey);
            }

            if (inTokenAmount > MAX_INT64) {
                throw new Error("Token inputs exceeded max amount. Consider sending in small chunks");
            }
            if (txBuilder.transaction.inputs.length > MAX_INPUTS_OUTPUTS) {
                throw new Error("Too many inputs. Consider consolidating transactions or reduce the send amount.");
            }
            
            if (inTokenAmount == outTokenAmount) {
                return Array.from(usedKeys.values());
            }
            if (inTokenAmount > outTokenAmount) {
                // change
                txBuilder.to(keys.changeKeys[keys.changeKeys.length - 1].address, Transaction.DUST_AMOUNT, token, inTokenAmount - outTokenAmount);
                return Array.from(usedKeys.values());
            }
        }
    }

    throw new Error("Not enough token balance");
}

async function finalizeTransaciton(tx: TransactionBuilder, privKeys: PrivateKey[]) {
    let tip = await rostrumProvider.getBlockTip();
    return tx.lockUntilBlockHeight(tip.height).sign(privKeys).build();
}

export async function broadcastTransaction(txHex: string) {
    return await rostrumProvider.broadcast(txHex);
}
