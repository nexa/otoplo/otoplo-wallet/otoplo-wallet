import { BarcodeFormat, BarcodeScanner, LensFacing } from "@capacitor-mlkit/barcode-scanning";
import { Dialog } from "@capacitor/dialog";
import { isMobilePlatform } from "./common.utils";

export async function startMobileScanner(handleScan: (data: string) => void) {
  if (!isMobilePlatform()) {
    return;
  }

  const { camera } = await BarcodeScanner.requestPermissions();
  if (camera !== 'granted' && camera !== 'limited') {
    await Dialog.alert({ title: 'Permission denied', message: 'Please grant camera permission to use the barcode scanner.' });
    return;
  }

  document.querySelector('body')!.classList.add('barcode-scanning-active');
  document.getElementById('barcode-scanning-modal')!.classList.remove('barcode-scanning-modal-hidden');
  document.getElementById('barcode-scanning-modal')!.classList.add('barcode-scanning-modal');

  const squareElementBoundingClientRect = document.getElementById('barcode-square')!.getBoundingClientRect();
  const scaledRect = squareElementBoundingClientRect
    ? {
        left: squareElementBoundingClientRect.left * window.devicePixelRatio,
        right: squareElementBoundingClientRect.right * window.devicePixelRatio,
        top: squareElementBoundingClientRect.top * window.devicePixelRatio,
        bottom: squareElementBoundingClientRect.bottom * window.devicePixelRatio,
        width: squareElementBoundingClientRect.width * window.devicePixelRatio,
        height: squareElementBoundingClientRect.height * window.devicePixelRatio,
      }
    : undefined;
  const detectionCornerPoints = scaledRect
    ? [
        [scaledRect.left, scaledRect.top],
        [scaledRect.left + scaledRect.width, scaledRect.top],
        [scaledRect.left + scaledRect.width, scaledRect.top + scaledRect.height],
        [scaledRect.left, scaledRect.top + scaledRect.height],
      ]
    : undefined;

  const listener = await BarcodeScanner.addListener('barcodesScanned',
    async (result) => {
      const cornerPoints = result.barcodes[0].cornerPoints;
      if (detectionCornerPoints && cornerPoints) {
        if (
          detectionCornerPoints[0][0] > cornerPoints[0][0] ||
          detectionCornerPoints[0][1] > cornerPoints[0][1] ||
          detectionCornerPoints[1][0] < cornerPoints[1][0] ||
          detectionCornerPoints[1][1] > cornerPoints[1][1] ||
          detectionCornerPoints[2][0] < cornerPoints[2][0] ||
          detectionCornerPoints[2][1] < cornerPoints[2][1] ||
          detectionCornerPoints[3][0] > cornerPoints[3][0] ||
          detectionCornerPoints[3][1] < cornerPoints[3][1]
        ) {
          return;
        }
      }

      await closeMobileScanner()
      handleScan(result.barcodes[0].rawValue);
    }
  );

  await BarcodeScanner.startScan({ formats: [BarcodeFormat.QrCode], lensFacing: LensFacing.Back });
}

export async function closeMobileScanner() {
  await BarcodeScanner.removeAllListeners();
  document.getElementById('barcode-scanning-modal')!.classList.remove('barcode-scanning-modal');
  document.getElementById('barcode-scanning-modal')!.classList.add('barcode-scanning-modal-hidden');
  document.querySelector('body')!.classList.remove('barcode-scanning-active');
  await BarcodeScanner.stopScan();
}
