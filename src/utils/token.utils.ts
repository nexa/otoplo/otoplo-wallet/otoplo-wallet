import { NftEntity, TokenEntity } from "../models/db.entities";
import { dbProvider } from "../app/App";
import { rostrumProvider } from "../providers/rostrum.provider";
import { getAddressBuffer, isNullOrEmpty } from "./common.utils";
import JSZip from "jszip";
import { Address, AddressType, CommonUtils, GroupIdType, GroupToken, Networks } from "libnexa-ts";
import TokenProvider from "../providers/token.provider";

const audioExtensions = ['.ogg', '.mp3']
const videoExtensions = ['.avif', '.webp', '.mp4', '.mpeg', '.mpg', '.webm'];
const imageExtensions = ['.svg', '.gif', '.png', '.apng', '.jpg', '.jpeg'];

export function tokenIdToHex(token: string) {
    if (CommonUtils.isHexa(token)) {
        return token;
    }
    return getAddressBuffer(token).toString('hex');
}

export async function fetchAndSaveNFT(token: string, parent: string, time: number, collection: string) {
    try {
        let exist = await dbProvider.isNftExist(token);
        if (exist) {
            return;
        }

        let hexId = tokenIdToHex(token);
        // nifty nfts give no information, must check if nifty collection subgroup
        let nft = "";
        if (TokenProvider.isNiftySubgroup(token)) {
            nft = await TokenProvider.fetchNiftyNFT(hexId);
        } else {
            // need to get genesis for data url, a field required in NRC-3 NFTs
            let genesis = await rostrumProvider.getTokenGenesis(token);
            if (genesis.document_url) {
                nft = await TokenProvider.fetchZip(genesis.document_url);
            } else {
                return;
            }
        }

        let zip = await JSZip.loadAsync(nft, { base64: true });
        let info = zip.file('info.json');
        let infoJson = '';
        if (info) {
            infoJson = await info.async('base64');
        }

        let pubImg = zip.file(/^public\./);
        let frontImg = zip.file(/^cardf\./);
        let preview = '';
        if (!isNullOrEmpty(pubImg)) {
            preview = classifyNftType(pubImg[0].name) != 'image' ? 'media' : await pubImg[0].async('base64');
        } else if (!isNullOrEmpty(frontImg)) {
            preview = classifyNftType(frontImg[0].name) != 'image' ? 'media' : await frontImg[0].async('base64');
        }

        let nftEntity: NftEntity = {
            addedTime: time,
            parentGroup: parent,
            token: token,
            tokenIdHex: hexId,
            zipData: nft,
            info: infoJson,
            preview: preview,
            collection: collection
        }
        await dbProvider.saveNft(nftEntity);
    } catch (e) {
        console.log('failed to fetch NFT');
    }
}

export function classifyNftType(filename: string) {
    let extension = filename.slice(filename.lastIndexOf('.')).toLowerCase();

    if (videoExtensions.includes(extension)) {
        return 'video';
    } else if (audioExtensions.includes(extension)) {
        return 'audio';
    } else if (imageExtensions.includes(extension)) {
        return 'image';
    } else {
        return 'unknown';
    }
}

function getIconData(icon: string, documentUrl: string) {
    if (typeof icon === 'string') {
        if (icon.startsWith('http')) {
            return icon.replace('http://', 'https://');
        }
        if (icon.startsWith('ipfs://')) {
            return icon;
        }
        let url = new URL(documentUrl);
        return `${url.origin}${icon}`;
    }
    return "";
}

export async function getTokenInfo(token: string) {
    try {
        let genesis = await rostrumProvider.getTokenGenesis(token);
        let groupId = Buffer.from(genesis.token_id_hex, 'hex');
        let parent = "";
        let iconUrl = "";

        if (genesis.op_return_id == GroupIdType.NRC2 || genesis.op_return_id == GroupIdType.NRC3 || TokenProvider.isNiftySubgroup(token)) {
            return false;
        }

        if (GroupToken.isSubgroup(groupId)) {
            parent = new Address(GroupToken.getParentGroupId(groupId), Networks.defaultNetwork, AddressType.GroupIdAddress).toString();
        }

        if (genesis.document_url) {
            try {
                if (genesis.op_return_id == GroupIdType.NRC1) {
                    let token_zip = await TokenProvider.fetchZip(genesis.document_url);
                    let zip = await JSZip.loadAsync(token_zip, { base64: true });
                    let info = zip.file('info.json');
                    if (info) {
                        let infoData = await info.async('string');
                        let infoJson = JSON.parse(infoData);
                        iconUrl = getIconData(infoJson[0]?.icon, genesis.document_url);
                    }
                } else {
                    let infoJson = await TokenProvider.fetchDoc(genesis.document_url);
                    iconUrl = getIconData(infoJson[0]?.icon, genesis.document_url);
                }
            } catch (e) {
                console.error("Failed to load metadata", e)
            }
        }

        let tokenEntity: TokenEntity = {
            token: genesis.group,
            tokenIdHex: genesis.token_id_hex,
            decimals: genesis.decimal_places ?? 0,
            parentGroup: parent,
            name: genesis.name ?? "",
            ticker: genesis.ticker ?? "",
            addedTime: 0,
            iconUrl: iconUrl
        };

        return tokenEntity;
    } catch (e) {
        console.error(e)
        return false;
    }
}
