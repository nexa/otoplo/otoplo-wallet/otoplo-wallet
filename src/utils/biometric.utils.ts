import { BiometricAuth, BiometryType } from "@aparajita/capacitor-biometric-auth";
import { SecureStorage } from "@aparajita/capacitor-secure-storage";
import faceid from '../assets/img/faceid.svg';
import fingerprint from '../assets/img/fingerprint.svg';
import biometric from '../assets/img/biometric.svg';
import StorageProvider from "../providers/storage.provider";

interface BiometryInfo {
  title: string;
  type: number;
  icon: string;
}

export interface BiometricData {
  info: BiometryInfo;
  pw: string;
}

export const biometricTypes: BiometryInfo[] = [
  {
    title: 'None',
    type: BiometryType.none,
    icon: ""
  },
  {
    title: 'Touch ID',
    type: BiometryType.touchId,
    icon: fingerprint
  },
  {
    title: 'Face ID',
    type: BiometryType.faceId,
    icon: faceid
  },
  {
    title: 'Fingerprint',
    type: BiometryType.fingerprintAuthentication,
    icon: fingerprint
  },
  {
    title: 'Biometric Auth',
    type: BiometryType.faceAuthentication,
    icon: biometric
  },
  {
    title: 'Biometric Auth',
    type: BiometryType.irisAuthentication,
    icon: biometric
  },
];

export async function isBiometricAvailable() {
  let biometric = await BiometricAuth.checkBiometry();
  return biometric.isAvailable;
}

export async function getEnrolledBiometricData(): Promise<BiometricData | undefined> {
  let biometric = await BiometricAuth.checkBiometry();
  if (!biometric.isAvailable) {
    return;
  }
  
  let useBiometric = await StorageProvider.getUseBiometric();
  if (useBiometric) {
    let pw = await getSecuredPassword();
    if (pw) {
      return { info: biometricTypes[biometric.biometryType], pw: `${pw}`};
    }
  }
}

export async function getSecuredPassword() {
  return await SecureStorage.get('otoplo_pw', false, false);
}

export async function setSecuredPassword(pw: string) {
  return await SecureStorage.set('otoplo_pw', pw, false, false);
}

export async function deleteSecuredPassword() {
  return await SecureStorage.remove('otoplo_pw', false);
}
