import StorageProvider from "../providers/storage.provider";
import { ContractEntity, TransactionEntity } from "../models/db.entities";
import { dbProvider } from "../app/App";
import { rostrumProvider } from "../providers/rostrum.provider";
import { classifyTransaction } from "./wallet.utils";
import { VaultInfo } from "../wallet/vault/Vault";
import { getAddressBuffer } from "./common.utils";
import { Address, BNExtended, BufferReader, BufferWriter, Hash, HDPrivateKey, Opcode, PublicKey, Script, ScriptOpcode } from "libnexa-ts";

const HODL_FIRST_BLOCK = 274710;
const HODL_SCRIPT_PREFIX =  "0014461ad25081cb0119d034385ff154c8d3ad6bdd76";

export function generateHodlKey(accountKey: HDPrivateKey, index: number) {
  return accountKey.deriveChild(0, false).deriveChild(index, false);
}

export function getHodlTemplate() {
  return Script.empty()
    .add(Opcode.OP_FROMALTSTACK).add(Opcode.OP_DROP)
    .add(Opcode.OP_FROMALTSTACK).add(Opcode.OP_CHECKLOCKTIMEVERIFY).add(Opcode.OP_DROP)
    .add(Opcode.OP_FROMALTSTACK).add(Opcode.OP_CHECKSIGVERIFY);
}

export function getHodlTemplateHash() {
  let template = getHodlTemplate();
  return Hash.sha256ripemd160(template.toBuffer());
}

export function generateHodlConstraint(pubKey: PublicKey) {
  return Script.empty().add(pubKey.toBuffer());
}

export function getHodlConstraintHash(pubKey: PublicKey) {
  let constraint = generateHodlConstraint(pubKey);
  return Hash.sha256ripemd160(constraint.toBuffer());
}

export function generateVisibleArgs(args: number[]) {
  return args.map(arg => arg <= 16 ? ScriptOpcode.smallInt(arg) : BNExtended.fromNumber(arg).toScriptNumBuffer());
}

export function generateHodlAddress(pubKey: PublicKey, args: any[]) {
  if (args.length !== 2) {
    return undefined;
  }

  var templateHash = getHodlTemplateHash();
  var constraintHash = getHodlConstraintHash(pubKey);
  var visibleArgs = generateVisibleArgs(args);
  var address = Address.fromScriptTemplate(templateHash, constraintHash, visibleArgs);
  return address.toString();
}

export async function getHodlNextIndex() {
  let state = await StorageProvider.getHodlState();
  return state.idx + 1;
}

export async function saveHodlAddress(index: number, address: string) {
  let vault: ContractEntity = {address: address, type: 'vault', archive: 0, confirmed: 0, unconfirmed: 0};
  await StorageProvider.setHodlState({idx: index});
  await dbProvider.addLocalVault(vault);
  return true;
}

export async function getHodlVaults() {
  return await dbProvider.getLocalVaults(false);
}

export async function getHodlArchive() {
  let archives = await dbProvider.getLocalVaults(true);
  return archives?.map(a => a.address); 
}

export function getVaultBlockAndIndex(vaultAddress: string) {
  let buf = getAddressBuffer(vaultAddress);
  let scirptTemplateBuf = new BufferReader(buf).readVarLengthBuffer();
  let scirptTemplate = Script.fromBuffer(scirptTemplateBuf);
  let visibleArgs = scirptTemplate.chunks[0].opcodenum !== Opcode.OP_0
    ? new Script({chunks: scirptTemplate.chunks.slice(4)})
    : new Script({chunks: scirptTemplate.chunks.slice(3)});

  let block = BNExtended.fromScriptNumBuffer(visibleArgs.chunks[0].buf!).toNumber();
  let index = ScriptOpcode.isSmallIntOp(visibleArgs.chunks[1].opcodenum)
                ? ScriptOpcode.decodeOP_N(visibleArgs.chunks[1].opcodenum)
                : BNExtended.fromScriptNumBuffer(visibleArgs.chunks[1].buf!).toNumber();

  return {block: block, index: index};
}

export function estimateDateByFutureBlock(current: number, future: number) {
  var estimateMins = (future - current) * 2
  var time = new Date();
  time.setMinutes(time.getMinutes() + estimateMins);
  return time.toLocaleDateString();
}

export async function discoverVaults(addresses: string[], vaultAccountKey: HDPrivateKey) {
  let vaultsPromises: Promise<Set<string>>[] = [];
  for (let address of addresses) {
    let p = checkVaultsForAddress(address);
    vaultsPromises.push(p);
  }

  let res = await Promise.all(vaultsPromises);

  let maxIndex = 0;
  let hodls = new Set<string>();

  res.forEach(set => {
    set.forEach(hex => {
      let hodl = getVaultAddressAndIndex(vaultAccountKey, hex);
      if (hodl) {
        hodls.add(hodl[0]);
        maxIndex = Math.max(maxIndex, hodl[1]);
      }
    });
  });

  return {index: maxIndex, vaults: hodls};
}

async function checkVaultsForAddress(address: string): Promise<Set<string>> {
  let vaults = new Set<string>();

  let history = await rostrumProvider.getTransactionsHistory(address);
  for (let txHistory of history) {
    if (txHistory.height > 0 && txHistory.height < HODL_FIRST_BLOCK) {
      continue;
    }

    let tx = await rostrumProvider.getTransaction(txHistory.tx_hash);
    let hodls = tx.vout.filter(out => out.scriptPubKey.hex.startsWith(HODL_SCRIPT_PREFIX));
    for (let hodl of hodls) { 
      vaults.add(hodl.scriptPubKey.hex);
    }
  }
  
  return vaults;
}

export function getVaultAddressAndIndex(vaultAccountKey: HDPrivateKey, hex: string): [string, number] | null {
  let scirptTemplate = Script.fromHex(hex);
  var buf = new BufferWriter().writeVarLengthBuf(scirptTemplate.toBuffer()).toBuffer();
  var actualAddress = new Address(buf).toString();

  let args = scirptTemplate.chunks[0].opcodenum !== Opcode.OP_0
    ? new Script({chunks: scirptTemplate.chunks.slice(4)})
    : new Script({chunks: scirptTemplate.chunks.slice(3)});

  let block = BNExtended.fromScriptNumBuffer(args.chunks[0].buf!).toNumber();
  let index = ScriptOpcode.isSmallIntOp(args.chunks[1].opcodenum)
                ? ScriptOpcode.decodeOP_N(args.chunks[1].opcodenum)
                : BNExtended.fromScriptNumBuffer(args.chunks[1].buf!).toNumber();

  var visibleArgs = [block, index];
  var key = generateHodlKey(vaultAccountKey, index);
  var expectedAddress = generateHodlAddress(key.publicKey, visibleArgs);

  return actualAddress === expectedAddress ? [expectedAddress, index] : null;
}

export async function fetchVaultTransactions(address: string) {
  let txHistory = await rostrumProvider.getTransactionsHistory(address);

  let transactions: Promise<TransactionEntity>[] = [];
  for (let tx of txHistory) {
    let txEntry = classifyTransaction(tx, [address]);
    transactions.push(txEntry);
  }

  return await Promise.all(transactions);
}

export async function fetchAllVaultsBalances(addresses: string[]) {
  let vaults: Promise<VaultInfo>[] = [];
  for (let address of addresses) {
    let vault = fetchVaultBalance(address);
    vaults.push(vault);
  }

  return await Promise.all(vaults);
}

async function fetchVaultBalance(address: string): Promise<VaultInfo> {
  let balance = await rostrumProvider.getBalance(address);
  return { address: address, balance: balance };
}