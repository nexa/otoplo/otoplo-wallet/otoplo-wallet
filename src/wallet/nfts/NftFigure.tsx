import { Figure } from "react-bootstrap";
import { NftDTO } from "../../models/db.entities";
import { useEffect, useState } from "react";
import { isMobileScreen, isNullOrEmpty } from "../../utils/common.utils";
import { Balance } from "../../models/wallet.entities";
import media from "../../assets/img/media.png";

export default function NftFigure({ nftDto, tokenBalance, onClick }: { nftDto: NftDTO, tokenBalance?: Balance, onClick: () => void }) {
  let isMobile = isMobileScreen();

  const [nftName, setNftName] = useState('No Title');
  const [nftImage, setNftImage] = useState('nft');

  useEffect(() => {
    async function loadData() {
      if (!tokenBalance) {
        // workaround for concurrent tx classification
        return;
      }

      if (!isNullOrEmpty(nftDto.info)) {
        try {
          let infoJson = Buffer.from(nftDto.info, 'base64').toString('utf-8');
          let infoObj = JSON.parse(infoJson);
          if (infoObj.title) {
            setNftName(infoObj.title);
          } else if(infoObj.name) {
            setNftName(infoObj.name);
          }
        } catch {
          setNftName("No Title");
        }
      }

      if (!isNullOrEmpty(nftDto.preview)) {
        if (nftDto.preview == 'media') {
          setNftImage(media);
        } else if (nftDto.preview != '') {
          let img = Buffer.from(nftDto.preview, 'base64');
          let url = URL.createObjectURL(new Blob([img]));
          setNftImage(url);
        }
      }
    }

    if (nftDto) {
      loadData().catch(e => {
          console.log(e)
      });
    }
  }, [nftDto]);

  let amtStr = "0";
  if (tokenBalance) {
    let amount = BigInt(tokenBalance.confirmed) + BigInt(tokenBalance.unconfirmed);
    amtStr = amount.toString();
  }

  if (amtStr == "0") {
    return;
  }

  return (
    <Figure className="m-3 nft-card" style={{ width: isMobile ? '40%' : '20%' }} onClick={onClick}>
      <Figure.Image src={nftImage} />
      <Figure.Caption className="text-white center">{nftName}</Figure.Caption>
    </Figure>
  )
}
