import { Alert, Button, Spinner, Table } from "react-bootstrap";
import { NftEntity, TransactionEntity } from "../../models/db.entities";
import { WalletKeys } from "../../models/wallet.entities";
import { ReactNode, useState } from "react";
import { currentTimestamp, parseAmountWithDecimals } from "../../utils/common.utils";
import { TxTokenType } from "../../utils/wallet.utils";
import { broadcastTransaction, buildAndSignTransferTransaction } from "../../utils/tx.utils";
import { dbProvider } from "../../app/App";
import { fetchBalance } from "../../store/slices/wallet.slice";
import { useAppDispatch } from "../../store/hooks";
import AuthAction from "../actions/AuthAction";
import ConfirmDialog from "../../components/dialogs/ConfirmDialog";
import InfoDialog from "../../components/dialogs/InfoDialog";
import { setLoader } from "../../store/slices/loader.slice";
import AddressField from "../../components/AddressField";
import SendDialog from "../../components/dialogs/SendDialog";
import { Transaction } from "libnexa-ts";

export default function NftSend({ nftEntity, keys }: { nftEntity: NftEntity, keys: WalletKeys }) {
  const [txMsg, setTxMsg] = useState<ReactNode>("");
  const [txErr, setTxErr] = useState<ReactNode>("");

  const [toAddress, setToAddress] = useState('');
  const [finalTx, setFinalTx] = useState<Transaction>(new Transaction());

  const [showInfoDialog, setShowInfoDialog] = useState(false);
  const [showAuth, setShowAuth] = useState(false);
  const [showSendDialog, setShowSendDialog] = useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);

  const [spinner, setSpinner] = useState<ReactNode>("");

  const dispatch = useAppDispatch();

  const closeConfirmationDialog = () => {
    setShowConfirmDialog(false);
  }

  const closeInfoDialog = () => {
    setTxErr("");
    setTxMsg("");
    setShowInfoDialog(false);
  }

  const cancelSendDialog = () => {
    setToAddress("");
    setFinalTx(new Transaction());
    setShowSendDialog(false);
  }

  const handleSend = async () => {
    try {
      setSpinner(<Spinner animation="border" size="sm"/>);
      let tx = await buildAndSignTransferTransaction(keys, toAddress, "1", false, nftEntity.token);
      setFinalTx(tx);
      setShowConfirmDialog(true);
    } catch (e) {
      if (e instanceof Error) {
        if (e.message.includes("errorMsg")) {
          let errMsg = JSON.parse(e.message);
          setTxErr(errMsg.errorMsg);      
        } else {
          setTxErr(e.message);
        }
      } else {
        setTxErr("Unable to fetch data. Please try again later and make sure the wallet is online.");
      }
      setShowInfoDialog(true);
    } finally {
      setSpinner("");
    }
  }

  const confirmSend = async () => {
    try {
      dispatch(setLoader({ loading: true }));
      let res = await broadcastTransaction(finalTx.serialize());
      setTxMsg(<><div>Success. Tx ID:</div><div style={{wordBreak: "break-all"}}>{res}</div></>);
      saveTx(res);
      dispatch(fetchBalance(true));
      cancelSendDialog();
    } catch (e) {
      setTxErr("Failed to send transaction. " + (e instanceof Error ? e.message : "Make sure the wallet is online."));
    } finally {
      closeConfirmationDialog();
      dispatch(setLoader({ loading: false }));
      setShowInfoDialog(true);
    }
  }

  const saveTx = (idem: string) => {
    try {
      let t: TransactionEntity = {
        txIdem: idem,
        txId: finalTx.id,
        payTo: toAddress,
        value: "0",
        time: currentTimestamp(),
        height: 0,
        extraGroup: "",
        fee: finalTx.getFee(),
        token: nftEntity.token,
        state: 'outgoing',
        tokenAmount: "1",
        txGroupType: TxTokenType.TRANSFER,
      }
      dbProvider.addLocalTransaction(t);
    } catch (e) {
      console.error("failed to save transaction", e);
    }
  }

  return (
    <>
      <Button className="mx-1" onClick={() => setShowSendDialog(true)}><i className="fa fa-upload"/> Send</Button>

      <SendDialog title="Send NFT" show={showSendDialog} onSend={handleSend} onClose={cancelSendDialog} disabled={spinner !== ""} sendBtn={spinner !== "" ? spinner : "Send"}>
        <AddressField disabled={spinner !== ""} value={toAddress} setValue={setToAddress}/>
      </SendDialog>

      <ConfirmDialog show={showConfirmDialog} onConfirm={() => setShowAuth(true)} onCancel={closeConfirmationDialog}>
        <Table>
          <tbody>
            <tr>
              <td>Send to:</td>
              <td style={{wordBreak: "break-all"}}>{toAddress}</td>
            </tr>
            <tr>
              <td>Fee:</td>
              <td>{parseAmountWithDecimals(finalTx.getFee(), 2)} NEXA</td>
            </tr>
          </tbody>
        </Table>
      </ConfirmDialog>

      <AuthAction show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={confirmSend} />

      <InfoDialog title={txErr !== "" ? "Error" : "Success"} show={showInfoDialog} onClose={closeInfoDialog}>
        <Alert show={txMsg !== ""} variant="success">
          {txMsg}
        </Alert>
        <Alert show={txErr !== ""} variant="danger">
          {txErr}
        </Alert>
      </InfoDialog>
    </>
  )
}
