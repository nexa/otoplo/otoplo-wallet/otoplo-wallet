import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import InfoDialog from "../../../components/dialogs/InfoDialog";
import StorageProvider from "../../../providers/storage.provider";
import { isMobilePlatform } from "../../../utils/common.utils";

export default function ReleaseNotes({ force = false }) {
  const [showDialog, setShowDialog] = useState(false);

  useEffect(() => {
    async function init() {
      let show = await StorageProvider.getShowReleaseNotes();
      setShowDialog(show);
    }

    if (force) {
      init();
    }
  }, []);

  const closeDialog = async () => {
    if (force) {
      await StorageProvider.setShowReleaseNotes(false);
    }
    setShowDialog(false)
  }

  return (
    <>
      { !force && <Button onClick={() => setShowDialog(true)}>Details</Button>}

      <InfoDialog scrollable title={`Otoplo Wallet ${import.meta.env.VITE_VERSION}`} show={showDialog} onClose={closeDialog} >
        <div className="larger mb-2 bold">What's New?</div>
        <div className="mb-4 light-txt">
          <div className="mb-3">Added NRC-1 tokens support.</div>
          <div className="mb-3">Added Nebula / NRC-3 NFTs support.</div>
          <div className="mb-3">Handle IPFS links for token descriptions and icons.</div>
          <div className="mb-3">Performance optimization and bug fixes.</div>
        </div>
        
        <div className='mb-1 mt-3 url'>
          <a href='https://gitlab.com/nexa/otoplo/otoplo-wallet/otoplo-wallet/-/blob/main/RELEASE-NOTES.md' target='_blank'>Full Release notes <i className="va fa-solid fa-arrow-up-right-from-square fa-xs"/></a>
        </div>
      </InfoDialog>
    </>
  )
}
