import { isMobilePlatform } from "../../../utils/common.utils";
import SettingsRow from "../SettingsRow";
import BiometricsSettings from "./BiometricsSettings";
import EnhancedProtection from "./EnhancedProtection";

export default function SecuritySettings() {

  let info = `Require authentication ${isMobilePlatform() ? '(password or biometrics) ' : ''}for sensitive actions such as confirm transactions and managing settings. Turning this off reduces security.`;

  return (
    <>
      <SettingsRow hr title="Enhanced Protection" info={info}>
        <EnhancedProtection/>
      </SettingsRow>
      { isMobilePlatform() && 
        <SettingsRow hr title="Use Biometrics" info="Use biometrics (e.g., Face ID, fingerprint) for fast and secure authentication instead of entering your password.">
          <BiometricsSettings/>
        </SettingsRow>
      }
    </>
  )
}
