import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import AuthAction from "../../actions/AuthAction";
import StorageProvider from "../../../providers/storage.provider";

export default function EnhancedProtection() {

  const [showAuth, setShowAuth] = useState(false);
  const [active, setActive] = useState(true);

  useEffect(() => {
    async function init() {
      let require = await StorageProvider.getRequireAuth();
      setActive(require);
    }

    init();
  }, [])

  const handleChange = async () => {
    await StorageProvider.setRequireAuth(!active);
    setActive(!active);
  }

  return (
    <>
      <Form.Switch checked={active} onChange={() => setShowAuth(true)}/>

      <AuthAction title="Authentication" show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={handleChange}/>
    </>
  )
}
