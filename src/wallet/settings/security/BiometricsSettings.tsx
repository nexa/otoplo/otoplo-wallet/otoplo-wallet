import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import StorageProvider from "../../../providers/storage.provider";
import AuthAction from "../../actions/AuthAction";
import { deleteSecuredPassword, getEnrolledBiometricData, isBiometricAvailable, setSecuredPassword } from "../../../utils/biometric.utils";

export default function BiometricsSettings() {

  const [showAuth, setShowAuth] = useState(false);
  const [active, setActive] = useState(false);
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    async function init() {
      let available = await isBiometricAvailable();
      setDisabled(!available);

      if (available) {
        let biometricData = await getEnrolledBiometricData();
        setActive(biometricData != undefined);
      }
    }

    init();
  }, [])

  const handleChange = async (pw: string) => {
    if (!active) {
      await setSecuredPassword(pw);
    } else {
      await deleteSecuredPassword();
    }
    await StorageProvider.setUseBiometric(!active);
    setActive(!active);
  }

  return (
    <>
      { disabled ? (
        <div className="bad small">Biometrics unavailable<br/>Check Device/App settings</div>
      ) : (
        <Form.Switch disabled={disabled} checked={active} onChange={() => setShowAuth(true)}/>
      )}

      <AuthAction title="Authentication" show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={handleChange} forcePw returnPw/>
    </>
  )
}
