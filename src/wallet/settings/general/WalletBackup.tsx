import { ReactElement, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/esm/Col';
import Row from 'react-bootstrap/esm/Row';
import { isMobileScreen } from '../../../utils/common.utils';
import AuthAction from '../../actions/AuthAction';
import InfoDialog from '../../../components/dialogs/InfoDialog';

export default function WalletBackup() {
  let isMobile = isMobileScreen();

  const [showAuth, setShowAuth] = useState(false);
  const [showSeed, setShowSeed] = useState(false);
  const [seedContent, setSeedContent] = useState<ReactElement[] | string>("");

  const showBackup = (decSeed: string) => {
    setSeedContent(buildSeed(decSeed.split(" ")));
    setShowSeed(true);
  }

  function buildSeed(words: string[]) {
    let content: ReactElement[] = [], columns: ReactElement[] = [];
    words.forEach((word, i) => {
      columns.push(
        <Col key ={i}>{i+1}<div>{word}</div></Col>
      );
        
      if((i+1) % (isMobile ? 3 : 4) === 0) {
        content.push(<Row key={i} className='py-2'>{columns}</Row>);
        columns = [];
      }
    });
    return content;
  }

  return (
    <>
      <Button onClick={() => setShowAuth(true)}>Show</Button>

      <AuthAction show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={showBackup} force/>

      <InfoDialog title='Backup Seed' modalClass='center' show={showSeed} onClose={() => setShowSeed(false)}>
        {seedContent}
      </InfoDialog>
    </>
  )
}
