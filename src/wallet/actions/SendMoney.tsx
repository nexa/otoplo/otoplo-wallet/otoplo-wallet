import { Alert, Button, FloatingLabel, Form, InputGroup, Spinner, Table } from "react-bootstrap";
import { Balance, WalletKeys } from "../../models/wallet.entities";
import { ChangeEvent, ReactElement, useState } from "react";
import { currentTimestamp, getRawAmount, isNullOrEmpty, parseAmountWithDecimals } from "../../utils/common.utils";
import { TxTokenType } from "../../utils/wallet.utils";
import { TokenEntity, TransactionEntity } from "../../models/db.entities";
import { broadcastTransaction, buildAndSignTransferTransaction } from "../../utils/tx.utils";
import { dbProvider } from "../../app/App";
import { useAppDispatch } from "../../store/hooks";
import { fetchBalance } from "../../store/slices/wallet.slice";
import SendDialog from "../../components/dialogs/SendDialog";
import AddressField from "../../components/AddressField";
import InfoDialog from "../../components/dialogs/InfoDialog";
import ConfirmDialog from "../../components/dialogs/ConfirmDialog";
import AuthAction from "./AuthAction";
import { setLoader } from "../../store/slices/loader.slice";
import { Transaction } from "libnexa-ts";

interface SendProps {
  balance: Balance;
  keys: WalletKeys;
  ticker: string;
  decimals: number;
  tokenEntity?: TokenEntity;
  tokenBalance?: Balance;
  isMobile?: boolean
}

export default function SendMoney({ balance, keys, ticker, decimals, tokenEntity, tokenBalance, isMobile }: SendProps) {
  const [showInfoDialog, setShowInfoDialog] = useState(false);
  const [showSendDialog, setShowSendDialog] = useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [showAuth, setShowAuth] = useState(false);

  const [spinner, setSpinner] = useState<ReactElement | string>("");

  const [txMsg, setTxMsg] = useState<ReactElement | string>("");
  const [txErr, setTxErr] = useState<ReactElement | string>("");

  const [toAddress, setToAddress] = useState('');
  const [amount, setAmount] = useState('');
  const [sendRawAmount, setSendRawAmount] = useState('');
  const [feeFromAmount, setFeeFromAmount] = useState(false);
  const [finalTx, setFinalTx] = useState<Transaction>(new Transaction());

  const dispatch = useAppDispatch();

  const amountRegx1 = decimals ? new RegExp(`^0\.[0-9]{1,${decimals}}$`) : /^[1-9][0-9]*$/;
  const amountRegx2 = decimals ? new RegExp(`^[1-9][0-9]*(\.[0-9]{1,${decimals}})?$`) : /^[1-9][0-9]*$/;

  const cancelSendDialog = () => {
    setToAddress("");
    setAmount("");
    setSendRawAmount("");
    setFinalTx(new Transaction());
    setFeeFromAmount(false);
    setShowSendDialog(false);
  }

  const closeInfoDialog = () => {
    setTxErr("");
    setTxMsg("");
    setShowInfoDialog(false);
  }

  const closeConfirmDialog = () => {
    setShowConfirmDialog(false);
  }

  const handleChangeAmount = (amount?: string) => {
    if (isNullOrEmpty(amount)) {
      setAmount('');
    } else if (amount === '0' || amountRegx1.test(amount) || amountRegx2.test(amount)) {
      setAmount(amount);
    }
  }

  const handleChangeFeeMode = (e: ChangeEvent<HTMLInputElement>) => {
    setTxErr("");
    setFeeFromAmount(e.target.checked);
  }

  const setMaxAmount = () => {
    let totalBalance = BigInt(balance.confirmed) + BigInt(balance.unconfirmed);
    if (tokenEntity) {
      totalBalance = 0n;
      if (tokenBalance) {
        totalBalance = BigInt(tokenBalance.confirmed) + BigInt(tokenBalance.unconfirmed);
      }
    } else {
      setFeeFromAmount(true);
    }
    setAmount(parseAmountWithDecimals(totalBalance, decimals).replaceAll(",", ""));
  }

  const handleSend = async () => {
    try {
      if (!amountRegx1.test(amount) && !amountRegx2.test(amount)) {
        throw new Error("Invalid Amount.");
      }

      let rawAmount = getRawAmount(amount, decimals);
      let amt = BigInt(rawAmount);
      let totalBalance = BigInt(balance.confirmed) + BigInt(balance.unconfirmed);
      if (tokenEntity) {
        totalBalance = 0n;
        if (tokenBalance) {
          totalBalance = BigInt(tokenBalance.confirmed) + BigInt(tokenBalance.unconfirmed);
        }
      }

      if (amt > totalBalance) {
        throw new Error("Insufficient balance.");
      }

      setSpinner(<Spinner animation="border" size="sm"/>);
      let tx = await buildAndSignTransferTransaction(keys, toAddress, amt.toString(), feeFromAmount, tokenEntity?.token);

      setSendRawAmount(tokenEntity ? rawAmount : tx.outputs[0].value.toString());
      setFinalTx(tx);
      setShowConfirmDialog(true);
    } catch (e) {
      if (e instanceof Error) {
        if (e.message.includes("errorMsg")) {
          let errMsg = JSON.parse(e.message);
          if (tokenEntity) {
            setTxErr(errMsg.errorMsg);
          } else {
            let err = <><div>Insufficient balance ({parseAmountWithDecimals(BigInt(balance.confirmed) + BigInt(balance.unconfirmed), 2)} NEXA).</div>
                      <div>Amount: {errMsg.amount} NEXA, Fee: {errMsg.fee} NEXA.</div></>
            setTxErr(err);
          }          
        } else {
          setTxErr(e.message);
        }
      } else {
        setTxErr("Unable to fetch data. Please try again later and make sure the wallet is online.");
      }
      setShowInfoDialog(true);
    } finally {
      setSpinner("");
    }
  }

  const confirmSend = async () => {
    try {
      dispatch(setLoader({ loading: true }));
      let res = await broadcastTransaction(finalTx.serialize());
      setTxMsg(<><div>Success. Tx ID:</div><div style={{wordBreak: "break-all"}}>{res}</div></>);
      saveTx(res)
      dispatch(fetchBalance(true));
      cancelSendDialog();
    } catch (e) {
      setTxErr("Failed to send transaction. " + (e instanceof Error ? e.message : "Make sure the wallet is online."));
    } finally {
      closeConfirmDialog();
      dispatch(setLoader({ loading: false }));
      setShowInfoDialog(true);
    }
  }

  const saveTx = (idem: string) => {
    try {
      let t: TransactionEntity = {
        txIdem: idem,
        txId: finalTx.id,
        payTo: toAddress,
        value: tokenEntity ? "0" : sendRawAmount,
        time: currentTimestamp(),
        height: 0,
        extraGroup: "",
        fee: finalTx.getFee(),
        token: tokenEntity?.token ?? "",
        state: 'outgoing',
        tokenAmount: tokenEntity ? sendRawAmount : "0",
        txGroupType: tokenEntity ? TxTokenType.TRANSFER : TxTokenType.NO_GROUP,
      }
      dbProvider.addLocalTransaction(t);
    } catch (e) {
      console.error("failed to save transaction", e);
    }
  }

  return (
    <>
      { isMobile ? (
        <div className='act-btn'>
          <Button onClick={() => setShowSendDialog(true)}><i className="fa fa-upload"/></Button>
          <br/>
          <span>Send</span>
        </div>
      ) : (
        <Button className='ms-2' onClick={() => setShowSendDialog(true)}><i className="fa fa-upload"/> Send</Button>
      )}

      <SendDialog title="Send" show={showSendDialog} onSend={handleSend} onClose={cancelSendDialog} disabled={spinner !== ""} sendBtn={spinner !== "" ? spinner : "Send"}>
        <AddressField disabled={spinner !== ""} value={toAddress} setValue={setToAddress} onAmountScan={handleChangeAmount}/>
        <InputGroup className="mb-2">
          <FloatingLabel  controlId="floatingInput" label="Amount">
            <Form.Control disabled={spinner !== ""} type="number" step={'0.01'} min='0.00' placeholder='0' value={amount} onChange={(e) => handleChangeAmount(e.target.value)}/>
          </FloatingLabel>
          <InputGroup.Text as='button' onClick={setMaxAmount}>Max</InputGroup.Text>
        </InputGroup>
        { !tokenEntity && <Form.Switch label="Subtract fee from amount" disabled={spinner !== ""} checked={feeFromAmount} onChange={handleChangeFeeMode}/> }
      </SendDialog>

      <ConfirmDialog show={showConfirmDialog} onConfirm={() => setShowAuth(true)} onCancel={closeConfirmDialog}>
        <Table>
          <tbody>
            { tokenEntity && 
              <tr>
                <td>Token:</td>
                <td style={{wordBreak: "break-all"}}>{tokenEntity.name ?? tokenEntity.token}</td>
              </tr>
            }
            <tr>
              <td>Pay to:</td>
              <td style={{wordBreak: "break-all"}}>{toAddress}</td>
            </tr>
            <tr>
              <td>Amount:</td>
              <td>{parseAmountWithDecimals(sendRawAmount, decimals)} {ticker}</td>
            </tr>
            <tr>
              <td>Fee:</td>
              <td>{parseAmountWithDecimals(finalTx.getFee(), 2)} NEXA</td>
            </tr>
            { !tokenEntity &&
              <tr>
                <td>Total:</td>
                <td>{parseAmountWithDecimals(BigInt(sendRawAmount) + BigInt(finalTx.getFee()), 2)} NEXA</td>
              </tr>
            }
          </tbody>
        </Table>
      </ConfirmDialog>

      <AuthAction show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={confirmSend} />

      <InfoDialog title={txErr !== "" ? "Error" : "Success"} show={showInfoDialog} onClose={closeInfoDialog}>
        <Alert show={txMsg !== ""} variant="success">
          {txMsg}
        </Alert>
        <Alert show={txErr !== ""} variant="danger">
          {txErr}
        </Alert>
      </InfoDialog>
    </>
  )
}
