import { ChangeEvent, useEffect, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { isPasswordValid } from "../../utils/seed.utils";
import ConfirmDialog from "../../components/dialogs/ConfirmDialog";
import { isMobilePlatform } from "../../utils/common.utils";
import { getEnrolledBiometricData } from "../../utils/biometric.utils";
import { BiometricAuth, BiometryError, BiometryErrorType } from "@aparajita/capacitor-biometric-auth";
import StorageProvider from "../../providers/storage.provider";
import { useAppDispatch } from "../../store/hooks";
import { setLoader } from "../../store/slices/loader.slice";

interface AuthProps {
  title?: string;
  show: boolean;
  force?: boolean;
  forcePw?: boolean;
  returnPw?: boolean;
  onClose: () => void;
  onAuthSuccess: (decSeed: string) => void;
}

export default function AuthAction({ title, show, force, forcePw, returnPw, onClose, onAuthSuccess }: AuthProps) {

  const [showDialog, setShowDialog] = useState(false);
  const [pw, setPw] = useState("");
  const [showPw, setShowPw] = useState(false);
  const [error, setError] = useState("");

  const dispatch = useAppDispatch();

  useEffect(() => {
    async function init() {
      if (show) {
        let require = await StorageProvider.getRequireAuth();
        if (!require && !force && !forcePw) {
          onAuthSuccess('');
          close();
          return;
        }

        if (forcePw || !isMobilePlatform()) {
          setShowDialog(true);
          return;
        }

        let biometricData = await getEnrolledBiometricData();
        if (biometricData) {
          await doBioAuth(biometricData.pw);
        } else {
          setShowDialog(true);
        }
      } else {
        close();
      }
    }
    
    init();
  }, [show]);

  const handleChangePw = (e: ChangeEvent<HTMLInputElement>) => {
    setError("");
    setPw(e.target.value);
  };

  const handleAuth = async (password: string) => {
    let decMn = await isPasswordValid(password);
    if (decMn) {
      onAuthSuccess(returnPw ? password : decMn);
      close();
    } else {
      setError("Incorrect password.");
    }
  }

  const doAuth = async () => {
    if (pw) {
      try {
        dispatch(setLoader({ loading: true }));
        await handleAuth(pw);
      } finally {
        dispatch(setLoader({ loading: false }));
      }
    }
  }

  const doBioAuth = async (pw: string) => {
    try {
      dispatch(setLoader({ loading: true }));
      await BiometricAuth.authenticate();
      await handleAuth(pw);
    } catch (error) {
      if (error instanceof BiometryError && error.code !== BiometryErrorType.userCancel) {
        setShowDialog(true);
      } else {
        close();
      }
    } finally {
      dispatch(setLoader({ loading: false }));
    }
  }

  const close = () => {
    setError("");
    setPw("");
    setShowPw(false);
    setShowDialog(false);
    onClose();
  }

  return (
    <ConfirmDialog title={title || "Unlock Wallet"} show={showDialog} onConfirm={doAuth} onCancel={close}>
      <p>Enter your password</p>
      <InputGroup>
        <Form.Control type={!showPw ? "password" : "text"} value={pw} placeholder="Password" autoFocus onChange={handleChangePw}/>
        <InputGroup.Text className='cursor' onClick={() => setShowPw(!showPw)}>{!showPw ? <i className="fa fa-eye" aria-hidden="true"></i> : <i className="fa fa-eye-slash" aria-hidden="true"></i>}</InputGroup.Text>
      </InputGroup>
      <span className='bad'>
        {error}
      </span>
    </ConfirmDialog>
  )
}
