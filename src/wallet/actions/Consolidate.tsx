import { ReactElement, useState } from 'react';
import { Alert, Button, Spinner, Table } from 'react-bootstrap';
import bigDecimal from 'js-big-decimal';
import { currentTimestamp, parseAmountWithDecimals } from '../../utils/common.utils';
import { Balance, WalletKeys } from '../../models/wallet.entities';
import { dbProvider } from "../../app/App";
import { TxTokenType } from '../../utils/wallet.utils';
import { TransactionEntity } from '../../models/db.entities';
import { broadcastTransaction, buildAndSignConsolidateTransaction } from '../../utils/tx.utils';
import { useAppDispatch } from '../../store/hooks';
import { fetchBalance } from '../../store/slices/wallet.slice';
import AuthAction from './AuthAction';
import InfoDialog from '../../components/dialogs/InfoDialog';
import { setLoader } from '../../store/slices/loader.slice';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import { Transaction } from 'libnexa-ts';

export default function Consolidate({ nexKeys, balance, isMobile }: { nexKeys: WalletKeys, balance: Balance, isMobile?: boolean }) {
  const [showAuth, setShowAuth] = useState(false);
  const [showInfoDialog, setShowInfoDialog] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [txErr, setTxErr] = useState("");
  const [spinner, setSpinner] = useState<ReactElement | string>("");
  const [txMsg, setTxMsg] = useState<ReactElement | string>("");

  const [finalTx, setFinalTx] = useState(new Transaction());
  const [toAddress, setToAddress] = useState("");
  const [totalAmount, setTotalAmount] = useState(new bigDecimal(0));
  const [totalFee, setTotalFee] = useState(new bigDecimal(0));

  const dispatch = useAppDispatch();

  const closeConfirmationDialog = () => {
    setSpinner("");
    setToAddress("");
    setFinalTx(new Transaction());
    setShowConfirmation(false);
  }

  const closeInfoDialog = () => {
    setTxErr("");
    setTxMsg("");
    setShowInfoDialog(false);
  }

  const consolidate = async () => {
    setSpinner(<Spinner animation="border" size="sm"/>);
    let toAddress = nexKeys.receiveKeys[nexKeys.receiveKeys.length - 1].address;

    try {
      let tx = await buildAndSignConsolidateTransaction(nexKeys);
      if (!tx.getChangeOutput()) {
        throw new Error("The transaction amount is too small to send after the fee has been deducted.");
      }
      setToAddress(toAddress);
      setFinalTx(tx);
      setTotalAmount(new bigDecimal(tx.outputAmount));
      setTotalFee(new bigDecimal(tx.getFee()));
      setShowConfirmation(true);
    } catch (e) {
      setTxErr(e instanceof Error ? e.message : "Unable to fetch data. Please try again later and make sure the wallet is online.");
      setShowInfoDialog(true);
    } finally {
      setSpinner("");
    }
  }

  const sendNexa = async () => {
    try {
      dispatch(setLoader({ loading: true }));
      let res = await broadcastTransaction(finalTx.serialize());
      setTxMsg(<><div>Success. Tx ID:</div><div style={{wordBreak: "break-all"}}>{res}</div></>);
      saveTx(res);
      dispatch(fetchBalance(true));
    } catch (e) {
      setTxErr("Failed to send transaction. " + (e instanceof Error ? e.message : "Make sure the wallet is online."));
    } finally {
      closeConfirmationDialog();
      dispatch(setLoader({ loading: false }));
      setShowInfoDialog(true);
    }
  }

  const saveTx = (idem: string) => {
    try {
      let t: TransactionEntity = {
        txIdem: idem,
        txId: finalTx.id,
        payTo: "Payment to yourself",
        value: "0",
        time: currentTimestamp(),
        height: 0,
        extraGroup: "",
        fee: finalTx.getFee(),
        token: "",
        state: 'both',
        tokenAmount: "0",
        txGroupType: TxTokenType.NO_GROUP,
      }
      dbProvider.addLocalTransaction(t);
    } catch (e) {
      console.error("failed to save transaction", e);
    }
  }

  let disabled = spinner !== "" || new bigDecimal(balance.confirmed).add(new bigDecimal(balance.unconfirmed)).compareTo(new bigDecimal(Transaction.DUST_AMOUNT)) < 0;

  return (
    <>
      { isMobile ? (
        <div className='act-btn'>
          <Button disabled={disabled} onClick={consolidate}>{spinner !== "" ? spinner : <i className="fa fa-repeat"/>}</Button>
          <br/>
          <span>Consolidate</span>
        </div>
      ) : (
        <Button disabled={disabled} className='mx-2'  onClick={consolidate}>{spinner !== "" ? spinner : <><i className="fa fa-repeat"/> Consolidate</>}</Button>
      )}

      <ConfirmDialog show={showConfirmation} onConfirm={() => setShowAuth(true)} onCancel={closeConfirmationDialog}>
        <div className='mb-2'>
          <b>This will consolidate multiple transaction UTXOs of your NEXA balance by sending them back to your wallet as one UTXO.
              This will prevent any wallet errors when trying to use too many inputs in a transaction.</b>
        </div>
        <Table>
          <tbody>
            <tr>
              <td>Pay to (own):</td>
              <td style={{wordBreak: "break-all"}}>{toAddress}</td>
            </tr>
            <tr>
              <td>Amount:</td>
              <td>{parseAmountWithDecimals(totalAmount.getValue(), 2)} NEXA</td>
            </tr>
            <tr>
              <td>Fee:</td>
              <td>{parseAmountWithDecimals(totalFee.getValue(), 2)} NEXA (3 sat/Byte)</td>
            </tr>
            <tr>
              <td>Total:</td>
              <td>{parseAmountWithDecimals(totalAmount.add(totalFee).getValue(), 2)} NEXA</td>
            </tr>
          </tbody>
        </Table>
      </ConfirmDialog>

      <AuthAction show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={sendNexa}/>

      <InfoDialog title={txErr !== "" ? "Error" : "Success"} show={showInfoDialog} onClose={closeInfoDialog}>
        <Alert show={txMsg !== ""} variant="success">
          {txMsg}
        </Alert>
        <Alert show={txErr !== ""} variant="danger">
          {txErr}
        </Alert>
      </InfoDialog>
    </>
  )
}
