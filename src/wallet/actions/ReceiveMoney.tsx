import { Button } from "react-bootstrap";
import { useState } from "react";
import QRDialog from "../../components/dialogs/QRDialog";

export default function ReceiveMoney({ address, isMobile }: { address: string, isMobile?: boolean }) {
  const [showQR, setShowQR] = useState(false);

  return (
    <>
      { isMobile ? (
        <div className='act-btn'>
          <Button onClick={() => setShowQR(true)}><i className="fa fa-download"/></Button>
          <br/>
          <span>Receive</span>
        </div>
      ) : (
        <Button className='ms-2' onClick={() => setShowQR(true)}><i className="fa fa-download"/> Receive</Button>
      )}

      <QRDialog title='Receive Address' value={address} show={showQR} onClose={() => setShowQR(false)}/>
    </>
  )
}
