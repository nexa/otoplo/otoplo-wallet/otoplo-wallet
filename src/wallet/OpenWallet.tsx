import React, { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import CreateWallet from './create/CreateWallet';
import RecoverWallet from './RecoverWallet';
import { isMobilePlatform } from '../utils/common.utils';
import { isPasswordValid } from '../utils/seed.utils';
import { BiometricAuth, BiometryError, BiometryErrorType, BiometryType } from '@aparajita/capacitor-biometric-auth';
import { BiometricData, biometricTypes, getEnrolledBiometricData } from '../utils/biometric.utils';
import { useAppDispatch } from '../store/hooks';
import { setLoader } from '../store/slices/loader.slice';

export default function OpenWallet({ setDecSeed }: { setDecSeed: Dispatch<SetStateAction<string>> }) {
  const [makeNew, setMakeNew] = useState(false);
  const [recover, setRecover] = useState(false);
  const [pwErr, setPwErr] = useState("");
  const [showPw, setShowPw] = useState(false);

  const [biometricData, setBiometricData] = useState<BiometricData>({ info: biometricTypes[0], pw: '' });

  const pwRef = useRef<HTMLInputElement>(null);

  const dispatch = useAppDispatch();

  useEffect(() => {
    async function init() {
      if (!isMobilePlatform()) {
        return;
      }
      
      let data = await getEnrolledBiometricData();
      if (data) {
        setBiometricData(data);
      }
    }

    init();
  }, []);

  const handleAuth = async (password: string, errorMsg: string) => {
    let decMn = await isPasswordValid(password);
    if (decMn) {
      setDecSeed(decMn);
    } else {
      setPwErr(errorMsg);
    }
  }

  const openWithBiometric = async () => {
    try {
      dispatch(setLoader({ loading: true }));
      await BiometricAuth.authenticate();
      await handleAuth(biometricData.pw, "Failed to authenticate.");
    } catch (error) {
      if (error instanceof BiometryError && error.code !== BiometryErrorType.userCancel) {
        console.log(error);
        setPwErr("Failed to authenticate, try again or use password.");
      }
    } finally {
      dispatch(setLoader({ loading: false }));
    }
  }

  const open = async () => {
    if (pwRef.current?.value) {
      try {
        dispatch(setLoader({ loading: true }));
        await handleAuth(pwRef.current.value, "Incorrect password.");
      } finally {
        dispatch(setLoader({ loading: false }));
      }
    }
  }

  const keyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if(event.key === 'Enter'){
      event.preventDefault()
      open();
    }
  }

  const show = () => setShowPw(!showPw);
  const newWallet = () => setMakeNew(true);
  const recoverWallet = () => setRecover(true);
  const cancelNewWallet = () => setMakeNew(false);
  const cancelRecoverWallet = () => setRecover(false);

  if (makeNew) {
    return (
      <CreateWallet cancelCreate={cancelNewWallet} setDecSeed={setDecSeed} />
    )
  }

  if (recover) {
    return (
      <RecoverWallet cancelRecover={cancelRecoverWallet} setDecSeed={setDecSeed} />
    )
  }

  return (
    <>
      <Card.Title className='pt-3'>Open Wallet</Card.Title>
      <hr/>
      <Form className='px-3 pb-3'>
        <Form.Group className="mb-2">
          <Form.Label>Unlock with your password</Form.Label>
          <InputGroup>
            <Form.Control type={!showPw ? "password" : "text"} ref={pwRef} placeholder="Password" autoFocus={!isMobilePlatform()} onKeyDown={keyDown}/>
            <InputGroup.Text className='cursor' onClick={show}>{!showPw ? <i className="fa fa-eye" aria-hidden="true"></i> : <i className="fa fa-eye-slash" aria-hidden="true"></i>}</InputGroup.Text>
            <Button onClick={open}>Open</Button>
          </InputGroup>
          <Form.Text className="text-red">
            {pwErr}
          </Form.Text>
        </Form.Group>

        <div className='mt-3'>
          <Button variant="outline-primary" className='mx-2' onClick={recoverWallet}>Recover from Seed</Button>
          <Button variant="outline-primary" className='mx-2' onClick={newWallet}>New Wallet</Button>
        </div>
      </Form>
      { biometricData.info.type !== BiometryType.none && 
        <div className='mb-4 mt-2'>
          <span className='p-3' onClick={openWithBiometric}>
            <img src={biometricData.info.icon} width={"40px"} /> Unlock with {biometricData.info.title}
          </span>
        </div>
      }
    </>
  )
}
