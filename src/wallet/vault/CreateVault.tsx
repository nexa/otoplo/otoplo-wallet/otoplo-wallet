import { ReactElement, useRef, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';
import bigDecimal from 'js-big-decimal';
import { currentTimestamp, getRawAmount, parseAmountWithDecimals } from '../../utils/common.utils';
import { Balance, WalletKeys } from '../../models/wallet.entities';
import { generateHodlAddress, generateHodlKey, getHodlNextIndex, getVaultBlockAndIndex, saveHodlAddress } from '../../utils/vault.utils';
import { TxTokenType } from '../../utils/wallet.utils';
import { dbProvider } from '../../app/App';
import { TransactionEntity } from '../../models/db.entities';
import { broadcastTransaction, buildAndSignTransferTransaction } from '../../utils/tx.utils';
import { Table } from 'react-bootstrap';
import { fetchBalance } from '../../store/slices/wallet.slice';
import { useAppDispatch } from '../../store/hooks';
import SendDialog from '../../components/dialogs/SendDialog';
import InfoDialog from '../../components/dialogs/InfoDialog';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import AuthAction from '../actions/AuthAction';
import { setLoader } from '../../store/slices/loader.slice';
import { HDPrivateKey, Transaction } from 'libnexa-ts';

interface CreateVaultProps {
  keys: WalletKeys;
  balance: Balance;
  vaultAccountKey: HDPrivateKey;
  heightVal: number;
  refreshVaults: () => Promise<void>;
}

export default function CreateVault({ keys, balance, vaultAccountKey, heightVal, refreshVaults }: CreateVaultProps) {
  const [showInfoDialog, setShowInfoDialog] = useState(false);
  const [showCreateDialog, setShowCreateDialog] = useState(false);
  const [byBlockHeight, setByBlockHeight] = useState(true);
  const [spinner, setSpinner] = useState<ReactElement | string>("");
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [showAuth, setShowAuth] = useState(false);

  const [finalTx, setFinalTx] = useState(new Transaction());
  const [toAddress, setToAddress] = useState("");
  const [totalFee, setTotalFee] = useState(0);

  const [createMsg, setCreateMsg] = useState<ReactElement | string>("");
  const [createErr, setCreateErr] = useState<ReactElement | string>("");
  const [estimateMsg, setEstimateMsg] = useState("");
  const [hodlBlock, setHodlBlock] = useState(0);

  const blockHeightRef = useRef<HTMLInputElement>(null);
  const dateRef = useRef<HTMLInputElement>(null);

  const dispatch = useAppDispatch();

  let nexBalance = new bigDecimal(balance.confirmed).add(new bigDecimal(balance.unconfirmed)).divide(new bigDecimal(100), 2);
  let nexAmount = 20;

  const closeInfoDialog = () => {
    setCreateMsg("");
    setCreateErr("");
    setShowInfoDialog(false);
  }

  const closeConfirmationDialog = () => {
    setShowConfirmDialog(false);
  }

  const cancelCreateDialog = () => {
    setHodlBlock(0);
    setEstimateMsg("");
    setSpinner("");
    setByBlockHeight(true);
    setShowCreateDialog(false);
    setTotalFee(0);
    setFinalTx(new Transaction());
    setToAddress("");
  }

  const changeCreateBy = () => {
    setByBlockHeight(!byBlockHeight);
    setHodlBlock(0);
    setEstimateMsg("");
    if (blockHeightRef.current) {
      blockHeightRef.current.value = '';
    }
    if (dateRef.current) {
      dateRef.current.value = '';
    }
  }

  const changeDate = () => {
    if (dateRef.current && heightVal > 0 && dateRef.current.valueAsNumber > Date.now()) {
      let estimateBlocks = Math.ceil((dateRef.current.valueAsNumber - Date.now()) / 1000 / 60 / 2);
      var block = heightVal + estimateBlocks;
      setHodlBlock(block);
      setEstimateMsg("Estimate block: " + block);
    } else {
      setHodlBlock(0);
      setEstimateMsg("");
    }
  }

  const changeBlock = () => {
    if (blockHeightRef.current && heightVal > 0 && blockHeightRef.current.valueAsNumber > heightVal) {
      var block = parseInt(blockHeightRef.current.value);
      var estimateMins = (block - heightVal) * 2
      var time = new Date();
      time.setMinutes(time.getMinutes() + estimateMins);
      setHodlBlock(block);
      setEstimateMsg("Estimate date: " + time.toLocaleDateString());
    } else {
      setHodlBlock(0);
      setEstimateMsg("");
    }
  }

  const handleCreate = async () => {
    if (nexBalance.compareTo(new bigDecimal(nexAmount)) < 0) {
      setCreateErr("Insufficient balance.");
      setShowInfoDialog(true);
      return;
    }
    if (hodlBlock <= 0) {
      return;
    }

    let block = hodlBlock;
    let idx = await getHodlNextIndex();
    let key = generateHodlKey(vaultAccountKey, idx);
    let address = generateHodlAddress(key.publicKey, [block, idx]) ?? '';

    setSpinner(<Spinner animation="border" size="sm"/>);
    try {
      let tx = await buildAndSignTransferTransaction(keys, address, getRawAmount(nexAmount, 2));
      setFinalTx(tx);
      setToAddress(address);
      setTotalFee(tx.getFee());
      setShowConfirmDialog(true);
    } catch (e) {
      console.log(e);
      setCreateErr(e instanceof Error ? e.message : "Unable to fetch data. Please try again later and make sure the wallet is online.");
      setShowInfoDialog(true);
    } finally {
      setSpinner("");
    }
  }

  const confirmCreate = async () => {
    try {
      dispatch(setLoader({ loading: true }));
      let res = await broadcastTransaction(finalTx.serialize());
      saveTx(res);
      dispatch(fetchBalance(true));
      var idx = getVaultBlockAndIndex(toAddress).index;
      await saveHodlAddress(idx, toAddress);

      setCreateMsg(<>
        <div className='center mb-2'>
          <div className='mb-1'><b>Vault created successfully!</b></div>
          <div style={{wordBreak: 'break-all'}}><b>Address:</b> {toAddress}</div>
          <div><b>Locked until block:</b> {hodlBlock}</div>
          <div><b>Index:</b> {idx}</div>
          <div style={{wordBreak: 'break-all'}}><b>Tx ID:</b> {res}</div>
        </div>
      </>);

      refreshVaults();
      cancelCreateDialog();
    } catch (e: any) {
      setCreateErr("Failed to send transaction. " + (e instanceof Error ? e.message : "Make sure the wallet is online."));
    } finally { 
      closeConfirmationDialog();
      dispatch(setLoader({ loading: false }));
      setShowInfoDialog(true);
    }
  }

  const saveTx = (idem: string) => {
    try {
      let t: TransactionEntity = {
        txIdem: idem,
        txId: finalTx.id,
        payTo: toAddress,
        value: getRawAmount(nexAmount, 2),
        time: currentTimestamp(),
        height: 0,
        extraGroup: "",
        fee: finalTx.getFee(),
        token: "",
        state: 'outgoing',
        tokenAmount: "0",
        txGroupType: TxTokenType.NO_GROUP,
      }
      dbProvider.addLocalTransaction(t);
    } catch (e) {
      console.error("failed to save transaction", e);
    }
  }

  let tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate()+1);
  let tomorrowDate = tomorrow.getFullYear()+'-'+(tomorrow.getMonth()+1 < 10 ? '0' : '')+(tomorrow.getMonth()+1)+'-'+tomorrow.getDate();

  return (
    <>
      <Button onClick={() => setShowCreateDialog(true)}>Create</Button>

      <SendDialog title='Create Vault' show={showCreateDialog} onSend={handleCreate} onClose={cancelCreateDialog} disabled={spinner !== ""} sendBtn={spinner !== "" ? spinner : "Create"}>
        <div className="mb-2">
          To create a Vault, 20 NEXA (+fees) will be sent to the Vault.<br/>
          After creation you will see the 20 NEXA in the Vault address balance.<br/><br/>
          <b>Current balance:</b> {nexBalance.getPrettyValue()} NEXA
        </div>
        <div className="mb-2">
          <span className='me-3'><b>Lock Until:</b></span>
          <Form.Check inline label="Block Height" name="lockby" type="radio" id="inline-radio-height" defaultChecked onChange={changeCreateBy}/>
          <Form.Check inline label="Future Date" name="lockby" type="radio" id="inline-radio-date" onChange={changeCreateBy}/>
        </div>
        <div className='center pb-2' style={{width: '50%'}}>
          { (byBlockHeight && <Form.Control placeholder={'e.g. '+(heightVal+1)} type="number" step='1' min={heightVal+1} ref={blockHeightRef} onChange={changeBlock}/>)
            || 
            <Form.Control className='center' type="date" min={tomorrowDate} ref={dateRef} onChange={changeDate}/>
          }
          <div className="mt-2">
            {estimateMsg}
          </div>
        </div>
      </SendDialog>

      <ConfirmDialog show={showConfirmDialog} onConfirm={() => setShowAuth(true)} onCancel={closeConfirmationDialog}>
        <Table>
          <tbody>
            <tr>
              <td>Pay to:</td>
              <td>New HODL Vault</td>
            </tr>
            <tr>
              <td>Locked until:</td>
              <td>Block {new bigDecimal(hodlBlock).getPrettyValue()}</td>
            </tr>
            <tr>
              <td>Amount:</td>
              <td>{nexAmount.toString()} NEXA</td>
            </tr>
            <tr>
              <td>Fee:</td>
              <td>{parseAmountWithDecimals(totalFee, 2)} NEXA (3 sat/Byte)</td>
            </tr>
            <tr>
              <td>Total:</td>
              <td>{parseAmountWithDecimals((nexAmount*100) + totalFee, 2)} NEXA</td>
            </tr>
          </tbody>
        </Table>
      </ConfirmDialog>

      <AuthAction show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={confirmCreate} />

      <InfoDialog title={createErr !== "" ? "Error" : "Success"} show={showInfoDialog} onClose={closeInfoDialog}>
        <Alert show={createMsg !== ""} variant="success">
          {createMsg}
        </Alert>
        <Alert show={createErr !== ""} variant="danger">
          {createErr}
        </Alert>
      </InfoDialog>
    </>
  )
}
