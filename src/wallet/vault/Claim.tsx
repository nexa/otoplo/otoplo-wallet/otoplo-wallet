import { ReactElement, useState } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import Button from 'react-bootstrap/Button';
import { currentTimestamp, parseAmountWithDecimals } from '../../utils/common.utils';
import bigDecimal from 'js-big-decimal';
import Alert from 'react-bootstrap/Alert';
import { Balance, WalletKeys } from '../../models/wallet.entities';
import { generateHodlConstraint, generateVisibleArgs, getHodlTemplate } from '../../utils/vault.utils';
import { TxTokenType } from '../../utils/wallet.utils';
import { dbProvider } from '../../app/App';
import { TransactionEntity } from '../../models/db.entities';
import { TxTemplateData, broadcastTransaction, buildAndSignConsolidateTransaction } from '../../utils/tx.utils';
import { Table } from 'react-bootstrap';
import { fetchBalance } from '../../store/slices/wallet.slice';
import { useAppDispatch } from '../../store/hooks';
import InfoDialog from '../../components/dialogs/InfoDialog';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import AuthAction from '../actions/AuthAction';
import { setLoader } from '../../store/slices/loader.slice';
import { HDPrivateKey, Transaction } from 'libnexa-ts';

interface ClaimProps {
  eta: string;
  balance: Balance;
  vaultKey: HDPrivateKey;
  vaultAddress: string;
  vaultInfo: { block: number, index: number };
  nexKeys: WalletKeys;
  refreshVaults: () => Promise<void>;
}

export default function Claim({ eta, balance, vaultKey, vaultAddress, vaultInfo, nexKeys, refreshVaults }: ClaimProps) {
  const [showInfoDialog, setShowInfoDialog] = useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [showAuth, setShowAuth] = useState(false);
  const [txErr, setTxErr] = useState("");
  const [spinner, setSpinner] = useState<ReactElement | string>("");
  const [txMsg, setTxMsg] = useState<ReactElement | string>("");

  const [finalTx, setFinalTx] = useState(new Transaction());
  const [toAddress, setToAddress] = useState("");
  const [totalAmount, setTotalAmount] = useState(new bigDecimal(0));
  const [totalFee, setTotalFee] = useState(new bigDecimal(0));

  const dispatch = useAppDispatch();

  let vaultBalance = BigInt(balance.confirmed) + BigInt(balance.unconfirmed);

  const closeConfirmDialog = () => {
    setSpinner("");
    setToAddress("");
    setFinalTx(new Transaction());
    setShowConfirmDialog(false);
  }

  const closeInfoDialog = () => {
    setTxErr("");
    setTxMsg("");
    setShowInfoDialog(false);
  }

  const claimVault = async () => {
    setSpinner(<Spinner animation="border" size="sm"/>);

    let toAddress = nexKeys.receiveKeys[nexKeys.receiveKeys.length - 1].address;
    let vaultKeys: WalletKeys = {receiveKeys: [{key: vaultKey, address: vaultAddress, balance: vaultBalance.toString(), tokensBalance: {}}], changeKeys: []};

    let templateData: TxTemplateData = {
      templateScript: getHodlTemplate(),
      constraintScript: generateHodlConstraint(vaultKey.publicKey),
      visibleArgs: generateVisibleArgs([vaultInfo.block, vaultInfo.index]),
      publicKey: vaultKey.publicKey,
    }

    try {
      let tx = await buildAndSignConsolidateTransaction(vaultKeys, toAddress, templateData);
      if (!tx.getChangeOutput()) {
        throw new Error("The transaction amount is too small to send after the fee has been deducted.");
      }
      setToAddress(toAddress);
      setFinalTx(tx);
      setTotalAmount(new bigDecimal(tx.outputAmount));
      setTotalFee(new bigDecimal(tx.getFee()));
      setShowConfirmDialog(true);
    } catch (e) {
      setTxErr(e instanceof Error ? e.message : "Unable to fetch data. Please try again later and make sure the wallet is online.");
      setShowInfoDialog(true);
    } finally {
      setSpinner("");
    }
  }

  const sendNexa = async () => {
    try {
      dispatch(setLoader({ loading: true }));
      let res = await broadcastTransaction(finalTx.serialize());
      setTxMsg(<><div>Success. Tx ID:</div><div style={{wordBreak: "break-all"}}>{res}</div></>);
      saveTx(res);
      dispatch(fetchBalance(true));
      refreshVaults();
    } catch (e) {
      setTxErr("Failed to send transaction. " + (e instanceof Error ? e.message : "Make sure the wallet is online."));
    } finally { 
      closeConfirmDialog();
      dispatch(setLoader({ loading: false }));
      setShowInfoDialog(true);
    }
  }

  const saveTx = (idem: string) => {
    try {
      let t: TransactionEntity = {
        txIdem: idem,
        txId: finalTx.id,
        payTo: toAddress,
        value: totalAmount.getValue(),
        time: currentTimestamp(),
        height: 0,
        extraGroup: "",
        fee: finalTx.getFee(),
        token: "",
        state: 'incoming',
        tokenAmount: "0",
        txGroupType: TxTokenType.NO_GROUP,
      }
      dbProvider.addLocalTransaction(t);
    } catch (e) {
      console.error("failed to save transaction", e);
    }
  }

  let disabled = spinner !== "" || eta !== "" || vaultBalance < BigInt(Transaction.DUST_AMOUNT);

  return (
    <>
      <Button disabled={disabled} className='mx-2' onClick={claimVault}>{spinner !== "" ? spinner : "Claim"}</Button>

      <ConfirmDialog show={showConfirmDialog} onConfirm={() => setShowAuth(true)} onCancel={closeConfirmDialog}>
        <Table>
          <tbody>
            <tr>
              <td>Pay to (own):</td>
              <td style={{wordBreak: "break-all"}}>{toAddress}</td>
            </tr>
            <tr>
              <td>Amount:</td>
              <td>{parseAmountWithDecimals(totalAmount.getValue(), 2)} NEXA</td>
            </tr>
            <tr>
              <td>Fee:</td>
              <td>{parseAmountWithDecimals(totalFee.getValue(), 2)} NEXA (3 sat/Byte)</td>
            </tr>
            <tr>
              <td>Total:</td>
              <td>{parseAmountWithDecimals(totalAmount.add(totalFee).getValue(), 2)} NEXA</td>
            </tr>
          </tbody>
        </Table>
      </ConfirmDialog>
      
      <AuthAction show={showAuth} onClose={() => setShowAuth(false)} onAuthSuccess={sendNexa}/>

      <InfoDialog title={txErr !== "" ? "Error" : "Success"} show={showInfoDialog} onClose={closeInfoDialog}>
        <Alert show={txMsg !== ""} variant="success">
          {txMsg}
        </Alert>
        <Alert show={txErr !== ""} variant="danger">
          {txErr}
        </Alert>
      </InfoDialog>
    </>
  )
}