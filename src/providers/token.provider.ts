import { CapacitorHttp, HttpResponseType } from "@capacitor/core";
import { TokenEntity } from "../models/db.entities";
import { getAddressBuffer } from "../utils/common.utils";
import { GroupToken } from "libnexa-ts";

export default class TokenProvider {

  private static readonly niftyEndpoint = import.meta.env.PROD ? "https://niftyart.cash/_public/" : "/_public/";

  public static readonly NIFTY_TOKEN: TokenEntity = {
    name: 'NiftyArt',
    ticker: 'NIFTY',
    iconUrl: 'https://niftyart.cash/td/niftyicon.svg',
    parentGroup: '',
    token: 'nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqqcg6kdm6f',
    tokenIdHex: 'cacf3d958161a925c28a970d3c40deec1a3fe06796fe1b4a7b68f377cdb90000',
    decimals: 0,
    addedTime: 0
  }

  public static async fetchNiftyNFT(id: string) {
    return await this.performGet<string>(this.niftyEndpoint + id, "arraybuffer");
  }

  public static isNiftySubgroup(group: string) {
    try {
      let addrBuf = getAddressBuffer(group);
      if (!GroupToken.isSubgroup(addrBuf)) {
          return false;
      }

      return addrBuf.subarray(0, 32).toString('hex') === this.NIFTY_TOKEN.tokenIdHex;
    } catch {
      return false;
    }
  }

  public static async fetchDoc(url: string) {
    // it is possible that legacy token description is stored on IPFS, check for this
    // example: ipfs://bafkvmicdm2vdjgieqvk3s5ykqlddtzd42yuy7voum4cifpknjvhv3uarwu
    url = this.translateIfIpfsUrl(url);
    return await this.performGet<any>(url, 'json');
  }
  
  public static async fetchZip(url: string) {
    // it is possible that NRC (Token / NFT) description is stored on IPFS, check for this
    // example: ipfs://bafkvmicdm2vdjgieqvk3s5ykqlddtzd42yuy7voum4cifpknjvhv3uarwu
    url = this.translateIfIpfsUrl(url);
    return await this.performGet<string>(url, "arraybuffer");
  }

  public static translateIfIpfsUrl(url: string, gateway = "https://ipfs.nebula.markets/") {
    // if ipfs gateway is implemented properly, you will be able to trim
    // the ipfs:// from the url and append the url to the gateway path
    // see https://docs.ipfs.tech/concepts/ipfs-gateway/#path for more info
    if (url?.startsWith("ipfs://")) {
      return gateway + url.substring(7);
    }
    return url;
  }

  private static async performGet<T>(url: string, responseType?: HttpResponseType) {
    try {
      let res = await CapacitorHttp.get({ url: url, responseType: responseType });
      if (res.status !== 200) {
        throw new Error(`Failed to perform GET. status: ${res.status}`);
      }
      return res.data as T;
    } catch (e) {
      console.error(e);
      throw new Error("Unexpected Error: " + (e instanceof Error ? e.message : "see log for details"));
    }
  }
}