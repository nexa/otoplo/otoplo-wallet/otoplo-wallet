import { ReactNode } from "react";
import { Button, Modal } from "react-bootstrap";

interface DialogProps {
  size?: 'sm' | 'lg' | 'xl';
  title?: string;
  show: boolean;
  onCancel: () => void;
  onConfirm: () => void;
  children: ReactNode;
}

export default function ConfirmDialog({ size, title, show, onCancel, onConfirm, children }: DialogProps) {
  
  return (
    <Modal data-bs-theme='dark' size={size} contentClassName='text-bg-dark' show={show} onHide={onCancel} backdrop="static" keyboard={false} centered>
      <Modal.Header closeButton>
        <Modal.Title>{title || "Confirmation"}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {children}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onCancel}>Cancel</Button>
        <Button onClick={onConfirm}>Confirm</Button> 
      </Modal.Footer>
    </Modal>
  )
}
