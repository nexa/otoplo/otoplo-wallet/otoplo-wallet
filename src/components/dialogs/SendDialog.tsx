import { ReactNode } from "react";
import { Button, Modal } from "react-bootstrap";

interface DialogProps {
  title: string;
  disabled?: boolean;
  show: boolean;
  onClose: () => void;
  onSend: () => void;
  sendBtn?: ReactNode;
  children: ReactNode;
}

export default function SendDialog({ title, disabled, show, onClose, onSend, sendBtn, children }: DialogProps) {

  return (
    <Modal data-bs-theme='dark' contentClassName='text-bg-dark' show={show} onHide={onClose} backdrop="static" keyboard={false} centered>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {children}
      </Modal.Body>
      <Modal.Footer>
        <Button disabled={disabled} variant="secondary" onClick={onClose}>Close</Button>
        <Button disabled={disabled} onClick={onSend}>{sendBtn || "Send"}</Button> 
      </Modal.Footer>
    </Modal>
  )
}
