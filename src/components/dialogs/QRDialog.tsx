import { QRCode } from "react-qrcode-logo";
import InfoDialog from "./InfoDialog";
import { Form, InputGroup } from "react-bootstrap";
import { copy } from "../../utils/common.utils";
import nex from '../../assets/img/nex.svg';

interface QRProps {
  title: string;
  value: string;
  show: boolean;
  onClose: () => void;
}

export default function QRDialog({ title, value, show, onClose }: QRProps) {

  return (
    <InfoDialog title={title} show={show} onClose={onClose} backdrop>
      <div className='center mb-3'>
        <QRCode value={value} size={220} logoImage={nex} logoWidth={35} logoPadding={1}/>
      </div>
      <InputGroup>
        <Form.Control className="smaller" disabled type="text" defaultValue={value}/>
        <InputGroup.Text as='button' onClick={() => copy(value, 'top-center')}><i className="fa-regular fa-copy"/></InputGroup.Text>
      </InputGroup>
    </InfoDialog>
  )
}
