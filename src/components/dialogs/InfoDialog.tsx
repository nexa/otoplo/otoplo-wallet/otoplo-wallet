import { ReactNode } from "react";
import { Button, Modal } from "react-bootstrap";

interface DialogProps {
  size?: 'sm' | 'lg' | 'xl';
  bodyClass?: string;
  modalClass?: string;
  scrollable?: boolean;
  title: string;
  show: boolean;
  onClose: () => void;
  backdrop?: boolean;
  children: ReactNode;
}

export default function InfoDialog({ size, bodyClass, modalClass, scrollable, title, show, onClose, backdrop, children }: DialogProps) {
  
  return (
    <Modal data-bs-theme='dark' scrollable={scrollable} size={size} contentClassName='text-bg-dark' className={modalClass} show={show} onHide={onClose} backdrop={backdrop ?? "static"} keyboard={false} centered>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body className={bodyClass}>
        {children}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onClose}>Close</Button>
      </Modal.Footer>
    </Modal>
  )
}
