import { ChangeEvent, useState } from 'react';
import { FloatingLabel, Form, InputGroup } from 'react-bootstrap'
import { isMobilePlatform } from '../utils/common.utils';
import { startMobileScanner } from '../utils/scanner.utils';
import { isValidNexaAddress } from '../utils/wallet.utils';
import QRScanner from './QRScanner';

interface AddressFieldProps {
  disabled: boolean;
  value: string;
  setValue: (val: string) => void;
  onAmountScan?: (amount?: string) => void;
}

export default function AddressField({ disabled, value, setValue, onAmountScan }: AddressFieldProps) {

  const [showScanDialog, setShowScanDialog] = useState(false);

  const [devices, setDevices] = useState<MediaDeviceInfo[]>([]);

  const handleChangeAddress = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const scanQR = async () => {
    if (isMobilePlatform()) {
      return await startMobileScanner(handleScan);
    }

    let devices = await navigator.mediaDevices.enumerateDevices();
    setDevices(devices.filter(d => d.kind == 'videoinput'));
    setShowScanDialog(true);
  }

  const handleScan = (data: string) => {
    if (data) {
      let uri = new URL(data);
      let address = uri.protocol + uri.pathname;

      if (onAmountScan) {
        let amount = uri.searchParams.get('amount');
        if (amount) {
          onAmountScan(amount);
        }
      }

      setShowScanDialog(false);
      setValue(!isValidNexaAddress(address) ? "Invalid Address" : address);
    }
  }

  const scanError = (err: any) => {
    setValue("");
    setShowScanDialog(false);
    console.error(err);
  }

  return (
    <>
      <InputGroup className="mb-2">
        <FloatingLabel controlId="floatingInput" label="To Address (must start with 'nexa:...')">
          <Form.Control disabled={disabled} type="text" placeholder='nexa:...' value={value} onChange={handleChangeAddress}/>
        </FloatingLabel>
        <InputGroup.Text as='button' onClick={scanQR}><i className="fa-solid fa-camera-retro"/></InputGroup.Text>
      </InputGroup>

      <QRScanner
        devices={devices} 
        showScanDialog={showScanDialog} 
        closeScanDialog={() => setShowScanDialog(false)} 
        handleScan={handleScan}
        scanError={scanError}
      />
    </>
  )
}
