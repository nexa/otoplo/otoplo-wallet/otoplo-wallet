import { IDetectedBarcode, Scanner } from "@yudiel/react-qr-scanner";
import { useState } from "react";
import { Dropdown } from "react-bootstrap";
import InfoDialog from "./dialogs/InfoDialog";

interface ScannerProps {
  devices: MediaDeviceInfo[];
  showScanDialog: boolean;
  closeScanDialog: () => void;
  handleScan: (data: string) => void;
  scanError: (err: any) => void;
}

export default function QRScanner({ devices, showScanDialog, closeScanDialog, handleScan, scanError }: ScannerProps) {
  const [selectedDevice, setSelectedDevice] = useState<string>('');

  const handleOnScan = (data: IDetectedBarcode[]) => {
    if (data.length > 0) {
      handleScan(data[0].rawValue);
    }
  }

  return (
    <>
      <InfoDialog title="Scan QR" size="sm" show={showScanDialog} onClose={closeScanDialog} bodyClass="center">
        <Scanner styles={{ container: { width: 250, marginBottom: "5px" }}} constraints={{ deviceId: selectedDevice, facingMode: 'environment' }} onError={scanError} onScan={handleOnScan}/>
        <Dropdown className="d-inline mx-2" onSelect={eventKey => setSelectedDevice(eventKey ?? '')}>
          <Dropdown.Toggle id="dropdown-autoclose-true" className="mt-3">
            Select Camera Device
          </Dropdown.Toggle>
          <Dropdown.Menu>
            { devices.map((d, i) => <Dropdown.Item key={i} eventKey={d.deviceId}>{d.label}</Dropdown.Item>) }
          </Dropdown.Menu>
        </Dropdown>
      </InfoDialog>
    </>
  )
}
